<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends \PHPUnit\Framework\TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    public function multiplyProvider(){
        return array(
            array(4, 2, 2),
            array(6, 3, 2),
            array(.72, .24, 3),
            array(.9, .3, 3),
            array(14, 7, 2)
        );
        /*
            Reason for failure:

                array(
                    estimated = .72, 
                    amount = .24, 
                    multiplyer = 3
                )
                estimated - amount = .48
                while condition checks if estimated - amount > .49, 
                and since it is not greater than .49, 
                it wont run and instead it will return amount rounded down to 0.
                ------------------------------------------------------------------------
                array(
                    estimated = .9, 
                    amount = .3, 
                    multiplyer = 3
                )
                guess = amount - 7 = 6.7
                Whenever the while conditions run, guess gets divided by 2.
                
                guess / 2 = 3.35
                At the first run, amount(.3) won't be greater than estimatedResult(.9),
                therefore we add guess(3.35) to amount, so now amount is 3.65.
                
                guess / 2 = 1.675
                At the second run amount(3.65) is greater than estimatedResult,
                therefore we substract amount guess from amount, so now amount it 1.975.

                guess / 2 = 0.8375
                At the third run amount(1.975) is still greater than estimatedResult,
                therefore we again substract guess from amount, so that amount now is 1.1375.

                And we've now reached the end of the loop. The loop requires that,
                estimatedResult - amount is greater than .49,
                but since estimatedResult(.9) - amount(1.1375) = .2375 is less than .49, it won't run.
                
                And we will therefore return amount(1.1375) rounded down to 1, which will fail the test, 
                because it wasn't the expected value 0.9

                ------------------------------------------------------------------------
                array(
                    estimated = 14, 
                    amount = 7, 
                    multiplyer = 2
                )
                guess = amount - 7 = 0
                while condition checks if guess != 0, 
                and since 7-7=0, 
                it wont run and instead it will return 7.
        */
    }
}
