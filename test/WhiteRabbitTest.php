<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit;

class WhiteRabbitTest extends \PHPUnit\Framework\TestCase
{
    /** @var WhiteRabbit */
    private $whiteRabbit;

    public function setUp()
    {
        $this->whiteRabbit = new WhiteRabbit();
        parent::setUp();
    }

    //SECTION FILE !
    /**
     * @dataProvider medianProvider
     */
    public function testMedian($expected, $file){
        $result = $this->whiteRabbit->findMedianLetterInFile($file);
        $this->assertTrue(in_array($result, $expected));
    }

    /*
        Changed tests for text 3 and text 5.

        First of all, our set of data (letters) is of size 26, so it's an even number.
        If there is en even number of items in a set, the median would would than be:
        1. the average of the 2 middle items(size/2 and (size/2)+1)
        2. Both of the middle items(size/2 and (size/2)+1)

        Since I need to return the letter whose occurrences are the median,
        The best and only approach in my opinion is no. 2.
        But instead of returning them both, I will just return the first(size/2)

        I made a visual presentation of all the letters and their occurrences, 
        so that I better could determine whether or not the tests expected the right letters.

        Text 3:
        1. z = 75
        2. q = 88
        3. x = 118
        4. j = 144
        5. v = 771
        6. k = 814
        7. b = 1611
        8. p = 1761
        9. y = 1884
        10. f = 2178
        11. g = 2187
        12. w = 2227
        13. m = 2244
        14. c = 2684
        15. u = 2825
        16. l = 4131
        17. d = 4213
        18. h = 5779
        19. s = 5800
        20. r = 5884
        21. i = 6285
        22. n = 6736
        23. o = 7149
        24. a = 7635
        25. t = 8851
        26. e = 11897
        Test 3 expected that the median letter would either be w = 2227 or g = 2187.
        Using my visual presentation, I can quickly see that w and g are not the medians.
        The median letter would either be letter no. 13 or letter no. 14.
        Since g is letter no. 11 and w is letter no. 12, i can decide that none of them is the median.
        --------------------------------------------------------------------------------------------------------
        Text 5:
        1. q = 698
        2. z = 858
        3. x = 877
        4. j = 1131
        5. v = 5860
        6. k = 6199
        7. b = 11514
        8. p = 11624
        9. y = 13827
        10. g = 14000
        11. c = 15902
        12. w = 17823
        13. f = 18122
        14. u = 18705
        15. m = 19857
        16. l = 28718
        17. d = 36645
        18. r = 41793
        19. s = 45473
        20. i = 49504
        21. o = 51296
        22. n = 53264
        23. h = 54184
        24. a = 63735
        25. t = 65270
        26. e = 91867
        Test 3 expected that the median letter would either be z = 858.
        I decided immediatly that this one was wrong. 2 reasons for that:
        1. z is letter no. 2, and therefore nowhere near being the median
        2. With a letter count of 858, it is very unlikely for z being the median letter, since our data set is sorted by occurrences.

    */
    public function medianProvider(){
        return array(
            array(array(array("letter" => "m", "count" => 9240), array("letter" => "f", "count" => 9095)), __DIR__ ."/../txt/text1.txt"),
            array(array(array("letter" => "w", "count" => 13333), array("letter" => "m", "count" => 12641)), __DIR__ ."/../txt/text2.txt"),
            array(array(array("letter" => "m", "count" => 2244), array("letter" => "c", "count" => 2684)), __DIR__ ."/../txt/text3.txt"),
            array(array(array("letter" => "w", "count" => 3049)), __DIR__ ."/../txt/text4.txt"),
            array(array(array("letter" => "f", "count" => 18122)), __DIR__ ."/../txt/text5.txt")
        );
    }
}
