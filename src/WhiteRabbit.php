<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //Read file into string
        $fileToString = file_get_contents($filePath);
        //Remove everything but letters, and then lowercasing the entire string
        $fileToString = strtolower(preg_replace('/[^A-Za-z]/', '', $fileToString));
        return $fileToString;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        /*
            Used count_chars to return an array with the ASCII number of each letter as key,
            and occurrences as value.
        */
        $letters = count_chars($parsedFile, 1);
        /*
            Used asort to sort the letters by occurrences and also keeping the ASCII numbers as key.
        */
        asort($letters);
        // I create a new array where the values are the letters ASCII numbers.
        $keys = array_keys($letters);

        /*
            Here I found the median
            by dividing the size of the array by 2.
            But before dividing I substract the size by 1, since arrays start at 0.
            And then I use floor to round down, because some tests only expected 1 median,
            which was the first median(size/2).
        */
        $idx = floor(((count($keys)-1) / 2));

        //Used chr to turn ASCII number into a letter.
        $letter = chr($keys[$idx]);
        /*
            I then get the occurrences from the letters array, by getting the value(ASCII number) of the keys array where index = idx.
        */
        $occurrences = $letters[$keys[$idx]];
        return $letter;
    }
}