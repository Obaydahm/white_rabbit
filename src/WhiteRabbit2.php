<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        $coins = array(
            '100'   => 0,
            '50'   => 0,
            '20'   => 0,
            '10'  => 0,
            '5'  => 0,
            '2'  => 0,
            '1' => 0
          );
          
          foreach($coins as $key =>  $value){
            /*
              1. We find the remainder by using modulo
              2. We subtract the remainder from amount
              3. We divide that by $key(which is the size of the coin/bill[1,2,5,10 etc.]) - and now we know how many of that given coin/bill we need.
            */
            $coins[$key] = ($amount - ($amount % $key)) / $key;

            //4. Assign the remainder to amount
            $amount = $amount % $key;
          }//5. repeat
          return $coins;
    }
}